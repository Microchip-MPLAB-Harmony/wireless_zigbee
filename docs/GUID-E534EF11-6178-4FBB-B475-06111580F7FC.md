# ZDO_GetNodeDescriptor Function

## C

```c
const NodeDescriptor_t* ZDO_GetNodeDescriptor(void);
```

## Description

 Gets the node descriptor of the device.

## Parameters

 None  

## Returns

 The node descriptor information of the current node.


