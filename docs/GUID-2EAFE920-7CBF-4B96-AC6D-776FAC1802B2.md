# ZDO_IncreaseJoinAttemptsAmount Function

## C

```c
void ZDO_IncreaseJoinAttemptsAmount(uint8_t amount);
```

## Description

 Increases join network attempts amount.

## Parameters

| Param | Description |
|:----- |:----------- |
| amount | amount of added attempts.  

## Returns

 None. 

